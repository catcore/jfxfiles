package co.simplon.alt6.jfxfiles;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FileOrderingTest {
    private List<Entry> entry = new ArrayList<>();
    File baseFolder = new File(System.getProperty("user.home"));

    File folder1 = new File(baseFolder, "atestFolder");
    File file1 = new File(baseFolder, "test.txt");
    File folder2 = new File(baseFolder, "testFolder");
    File file2 = new File(baseFolder, "btest.txt");

    public FileOrderingTest() {
        this.entry.add(new FolderEntry(folder1));
        this.entry.add(new FileEntry(file1));
        this.entry.add(new FolderEntry(folder2));
        this.entry.add(new FileEntry(file2));
    }

    @Test
    void byName() {
        FileOrdering.order(this.entry, new FileOrdering.FileOrderingEntry(FileOrdering.NAME));

        assertEquals(folder1, this.entry.get(0).file);
        assertEquals(file2, this.entry.get(1).file);
        assertEquals(file1, this.entry.get(2).file);
        assertEquals(folder2, this.entry.get(3).file);
    }

    @Test
    void byNameReversed() {
        FileOrdering.order(this.entry, new FileOrdering.FileOrderingEntry(FileOrdering.NAME, true));

        assertEquals(folder1, this.entry.get(3).file);
        assertEquals(file2, this.entry.get(2).file);
        assertEquals(file1, this.entry.get(1).file);
        assertEquals(folder2, this.entry.get(0).file);
    }

    @Test
    void byTypeAndName() {
        FileOrdering.order(this.entry, new FileOrdering.FileOrderingEntry(FileOrdering.ENTRY_TYPE, true), new FileOrdering.FileOrderingEntry(FileOrdering.NAME));

        assertEquals(folder1, this.entry.get(0).file);
        assertEquals(file2, this.entry.get(2).file);
        assertEquals(file1, this.entry.get(3).file);
        assertEquals(folder2, this.entry.get(1).file);
    }

    @Test
    void byTypeAndNameReversed() {
        FileOrdering.order(this.entry, new FileOrdering.FileOrderingEntry(FileOrdering.ENTRY_TYPE, true), new FileOrdering.FileOrderingEntry(FileOrdering.NAME, true));

        assertEquals(folder1, this.entry.get(1).file);
        assertEquals(file2, this.entry.get(3).file);
        assertEquals(file1, this.entry.get(2).file);
        assertEquals(folder2, this.entry.get(0).file);
    }
}
