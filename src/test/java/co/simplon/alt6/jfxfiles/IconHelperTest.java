package co.simplon.alt6.jfxfiles;

import org.junit.jupiter.api.Test;

import java.io.File;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class IconHelperTest {
    File baseFolder = new File(System.getProperty("user.home"));
    File file1 = new File(baseFolder, "test.png");
    File file2 = new File(baseFolder, "test.bin");
    File file3 = new File(baseFolder, "btest.txt");

    @Test
    void folderIcon() {
        assertEquals(IconHelper.getFolderIconPath(), IconHelper.getIconPath(new FolderEntry(baseFolder)));
    }

    @Test
    void pngIcon() {
        assertEquals(file1.toString(), IconHelper.getIconPath(new FileEntry(file1)));
    }

    @Test
    void binIcon() {
        assertEquals(IconHelper.getBinFileIconPath(), IconHelper.getIconPath(new FileEntry(file2)));
    }

    @Test
    void txtIcon() {
        assertEquals(IconHelper.getTextFileIconPath(), IconHelper.getIconPath(new FileEntry(file3)));
    }
}
