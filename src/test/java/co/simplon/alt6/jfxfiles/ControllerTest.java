package co.simplon.alt6.jfxfiles;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ControllerTest {
    Controller controller = new Controller();
    File baseFolder = new File(System.getProperty("user.home"));
    @Test
    void createFile() {
        controller.createFile("test.txt");

        File testFile = new File(baseFolder, "test.txt");

        assertTrue(testFile.exists());
        assertTrue(testFile.isFile());
    }

    @Test
    void createDirectory() {
        controller.createFolder("testFolder");

        File testFile = new File(baseFolder, "testFolder");

        assertTrue(testFile.exists());
        assertTrue(testFile.isDirectory());
    }

    @Test
    void enterParentFolder() {
        File homeFolder = controller.currentFolder;

        controller.enterParentFolder();

        assertEquals(homeFolder.getParentFile(), controller.currentFolder);

        assertTrue(controller.folderContent.stream().anyMatch(entry -> Objects.equals(entry.getName(), homeFolder.getName())));
    }

    @Test
    void enterFolderChild() {
        File homeFolder = controller.currentFolder;

        controller.enterFolder(homeFolder);

        controller.createFolder("test-folder");

        controller.enterChildFolder("test-folder");

        File subFolder = new File(homeFolder, "test-folder");

        assertEquals(subFolder, controller.currentFolder);
    }
}
