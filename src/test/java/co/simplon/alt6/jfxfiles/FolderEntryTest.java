package co.simplon.alt6.jfxfiles;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import java.io.File;

public class FolderEntryTest {

    @Test
    void testGetEntries() {
        File folder = new File(System.getProperty("user.home"));

        FolderEntry folderEntry = new FolderEntry(folder);

        Entry[] entries = folderEntry.getEntries();

        assertNotNull(entries);

        for (Entry entry : entries) {
            assertTrue(entry instanceof FolderEntry || entry instanceof FileEntry);
        }
    }
}