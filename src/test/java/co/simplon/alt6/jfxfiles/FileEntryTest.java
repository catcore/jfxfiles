package co.simplon.alt6.jfxfiles;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.junit.jupiter.api.Test;

public class FileEntryTest {

    File fileEntry = new File(System.getProperty("user.home"));

    @Test
    void readFileContent() {
        File testFile = new File(fileEntry, "test.txt");

        try {
            testFile.createNewFile();
            BufferedWriter writer = new BufferedWriter(new FileWriter(testFile));
            writer.write("welcome to file");
            writer.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        FileEntry testFileEntry = new FileEntry(testFile);
        assertEquals("welcome to file", testFileEntry.readFileContent().trim());

    }


}
