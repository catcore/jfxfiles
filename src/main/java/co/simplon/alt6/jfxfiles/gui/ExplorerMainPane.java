package co.simplon.alt6.jfxfiles.gui;

import co.simplon.alt6.jfxfiles.Controller;
import co.simplon.alt6.jfxfiles.Entry;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.util.List;
import java.util.Optional;

public class ExplorerMainPane extends BorderPane {
    final Controller controller;
    final Stage stage;
    final EntryListPane listPane;
    public ExplorerMainPane(Controller controller, Stage stage) {
        this.controller = controller;
        this.stage = stage;
        this.listPane = new EntryListPane(this);
    }

    /**
     * Add elements to the pane.
     */
    public void draw() {
        this.listPane.init();
        Button delete = new Button("Delete");
        delete.setOnAction(e -> {
            this.listPane.tryDeletingEntry();
        });

        Button rename = createRenameButton();
        Button createFile = createCreateFileButton();
        Button createFolder = createCreateFolderButton();

        Button copy = new Button("Copy");
        copy.setOnAction(e -> {
            if (!listPane.selectedRow.isEmpty()) {
                List<Entry> entries = listPane.selectedRow.stream()
                        .map(listPane.entryList::get)
                        .toList();

                controller.copyToClipboard(entries);
            }
        });

        Button paste = new Button("Paste");
        paste.setOnAction(e -> {
            controller.pasteFromClipboard();
        });

        Button back = new Button("Back");
        back.setOnAction(e -> {
            controller.enterParentFolder();
            this.listPane.draw();
        });

        HBox actionBar = new HBox();
        actionBar.setPadding(new Insets(15, 12, 15, 12));
        actionBar.setSpacing(10);
        actionBar.getChildren().addAll(back, createFile, createFolder, rename,
                delete, copy, paste);

        this.drawQuickAccess();

        this.setTop(actionBar);
        this.setCenter(this.listPane);
        this.listPane.draw();
    }

    /**
     * Add quick access buttons to the pane.
     */
    private void drawQuickAccess() {
        VBox quickAccess = new VBox();
        quickAccess.setPadding(new Insets(15, 12, 15, 12));
        quickAccess.setSpacing(10);

        for (Entry entry : controller.getCommonFolders()) {
            Button button = new Button(entry.getName());

            button.setOnMouseClicked(event -> {
                controller.enterFolder(entry.file);
                this.listPane.draw();
            });

            quickAccess.getChildren().add(button);
        }

        this.setLeft(quickAccess);
    }

    /**
     * @return "create folder" button
     */
    private Button createCreateFolderButton() {
        Button createFolder = new Button("Create Folder");
        createFolder.setOnAction(e -> {
            TextInputDialog dialog = new TextInputDialog();
            dialog.setTitle("Create Folder");
            dialog.setHeaderText("Enter a folder name:");
            Optional<String> result = dialog.showAndWait();

            result.ifPresent(fileName -> {
                controller.createFolder(fileName);
                this.listPane.draw();

                this.listPane.selectEntry(fileName);
            });
        });
        return createFolder;
    }

    /**
     * @return "create file" button
     */
    private Button createCreateFileButton() {
        Button createFile = new Button("Create File");
        createFile.setOnAction(e -> {
            TextInputDialog dialog = new TextInputDialog();
            dialog.setTitle("Create File");
            dialog.setHeaderText("Enter a file name:");
            Optional<String> result = dialog.showAndWait();

            result.ifPresent(fileName -> {
                controller.createFile(fileName);
                this.listPane.draw();

                this.listPane.selectEntry(fileName);
            });
        });
        return createFile;
    }

    /**
     * @return "create rename" button
     */
    private Button createRenameButton() {
        Button rename = new Button("Rename");
        rename.setOnAction(e -> {
            if (this.listPane.selectedRow.size() == 1) {
                Entry entry = this.listPane.entryList.get(this.listPane.selectedRow.get(0));

                TextInputDialog dialog = new TextInputDialog(entry.getName());
                dialog.setTitle("Rename");
                dialog.setHeaderText("Enter a new name:");
                Optional<String> result = dialog.showAndWait();

                result.ifPresent(fileName -> {
                    controller.renameFile(fileName, entry.file);
                    this.listPane.draw();

                    this.listPane.selectEntry(fileName);
                });
            }
        });
        return rename;
    }
}
