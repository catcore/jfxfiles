package co.simplon.alt6.jfxfiles.gui;

import co.simplon.alt6.jfxfiles.Entry;
import co.simplon.alt6.jfxfiles.FolderEntry;
import co.simplon.alt6.jfxfiles.IconHelper;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class EntryListPane extends ScrollPane {
    private final GridPane gridPane;
    private final ExplorerMainPane mainPane;
    /**
     * List of selected row index.
     */
    final List<Integer> selectedRow = new ArrayList<>();
    /**
     * Row index to Nodes map.
     */
    private final List<List<Node>> rowToNodes = new ArrayList<>();
    /**
     * Current entry list.
     */
    final List<Entry> entryList = new ArrayList<>();

    EntryListPane(GridPane pane, ExplorerMainPane mainPane) {
        super(pane);
        this.gridPane = pane;
        this.mainPane = mainPane;
    }

    public EntryListPane(ExplorerMainPane mainPane) {
        this(new GridPane(), mainPane);
    }

    /**
     * Initiate entry deletion process.
     */
    protected void tryDeletingEntry() {
        if (!selectedRow.isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Removing Files");
            alert.setHeaderText("Are you sure you want to delete those files?");

            StringBuilder builder = new StringBuilder();

            for (int i : selectedRow) {
                Entry entry = entryList.get(i);
                builder.append(entry.getName()).append("\n");
            }

            alert.setContentText(builder.toString());

            alert.showAndWait().ifPresent(but -> {
                if (but == ButtonType.OK) {
                    boolean changed = false;
                    for (int row : selectedRow) {
                        Entry entry = entryList.get(row);

                        if (mainPane.controller.deleteFile(entry.getName())) {
                            changed = true;
                        }
                    }

                    if (changed) {
                        this.draw();
                    }
                }
            });
        }
    }

    /**
     * Set default events
     */
    void init() {
        gridPane.setHgap(10);

        // Trigger removing when Del or Ctrl+BackSpace are pressed.
        this.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.DELETE || (event.isControlDown() && event.getCode() == KeyCode.BACK_SPACE)) {
                this.tryDeletingEntry();
            }
        });

        gridPane.setOnMouseClicked(e -> {
            if (e.getButton().equals(MouseButton.PRIMARY)) {
                Node target = (Node) e.getTarget();
                int row = -1;

                while (row == -1) {
                    try {
                        row = GridPane.getRowIndex(target);
                    } catch (Exception ex) {
                        target = target.getParent();
                    }
                }

                if (selectedRow.contains(row)) {
                    if (e.isControlDown()) {
                        this.unselectLine(row);
                    } else {
                        clearSelection();
                        selectLine(row);
                    }
                } else {
                    if (!e.isControlDown()) {
                        clearSelection();
                    }

                    selectLine(row);
                }

                // Only enable rename button if we are selecting only one element
//                rename.setDisable(selectedRow.size() != 1);
//
//                delete.setDisable(selectedRow.isEmpty());

                if (e.getClickCount() == 2) {
                    Entry entry = entryList.get(row);

                    if (entry instanceof FolderEntry folderEntry) {
                        mainPane.controller.enterFolder(folderEntry);
                        this.draw();
                    } else {
                        mainPane.controller.openFile(entry);
                    }
                }
            }
        });
    }

    /**
     * (Re-)draw entry list.
     */
    void draw() {
        gridPane.getChildren().clear();
        entryList.clear();
        this.setVvalue(0);
        this.setHvalue(0);
        rowToNodes.clear();
        selectedRow.clear();

        mainPane.stage.setTitle(this.mainPane.controller.currentFolder.toString());

        int i = 0;
        for (Entry entry : mainPane.controller.getEntries()) {
            entryList.add(entry);
            int j = i++;

            rowToNodes.add(new ArrayList<>());

            try {
                InputStream inputStream = IconHelper.getEntryIcon(entry);

                if (inputStream != null) {
                    Image image = new Image(inputStream);
                    ImageView imageView = new ImageView(image);
                    imageView.setFitHeight(45);
                    imageView.setFitWidth(45);
                    imageView.setPreserveRatio(true);
                    Pane imagePane = new Pane(imageView);

                    gridPane.add(imagePane, 0, j);

                    rowToNodes.get(j).add(imagePane);
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            var name = new Pane(new Label(entry.getName()));
            gridPane.add(name, 1, j);

            var creationTime = new Pane(new Label(entry.creationTime().toString()));
            gridPane.add(creationTime, 2, j);
            var accessTime = new Pane(new Label(entry.lastAccessTime().toString()));
            gridPane.add(accessTime, 3, j);
            var modificationTime = new Pane(new Label(entry.lastModifiedTime().toString()));
            gridPane.add(modificationTime, 4, j);
            var size = new Pane(new Label(entry.getSize() + " bytes"));
            gridPane.add(size, 5, j);

            rowToNodes.get(j).addAll(List.of(name, creationTime, accessTime, modificationTime, size));
        }
    }

    /**
     * Set entry background color
     * @param row entry row index
     * @param color background color
     */
    private void setLineColor(int row, Color color) {
        for (Node node : rowToNodes.get(row)) {
            if (node instanceof Pane label) {
                label.setBackground(new Background(new BackgroundFill(color, CornerRadii.EMPTY, Insets.EMPTY)));
            }
        }
    }

    /**
     * Set entry background color to "select color".
     * @param row entry row index
     */
    private void selectLine(int row) {
        setLineColor(row, Color.LIGHTBLUE);
        selectedRow.add(row);
    }

    /**
     * Set entry background color back to default.
     * @param row entry row index
     */
    private void unselectLine(int row) {
        setLineColor(row, Color.TRANSPARENT);
        selectedRow.remove((Integer) row);
    }

    /**
     * Set all entry background color back to default.
     */
    private void clearSelection() {
        for (int row : selectedRow) {
            setLineColor(row, Color.TRANSPARENT);
        }

        this.selectedRow.clear();
    }

    /**
     * Add entry to the selected list.
     * @param name entry name
     */
    void selectEntry(String name) {
        for (int i = 0; i < entryList.size(); i++) {
            Entry entry = entryList.get(i);

            if (entry.getName().equals(name)) {
                this.selectLine(i);
                break;
            }
        }
    }
}
