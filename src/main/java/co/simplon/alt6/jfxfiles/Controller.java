package co.simplon.alt6.jfxfiles;

import dev.dirs.UserDirectories;

import java.awt.*;
import java.awt.datatransfer.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * TODO
 */
public class Controller {
    /**
     * The current folder the controller is looking at.
     * Default value is the user home folder.
     */
    public File currentFolder;
    /**
     * All entries contained in the current folder.
     */
    List<Entry> folderContent = new ArrayList<>();

    public Controller() {
        this.enterFolder(new File(System.getProperty("user.home")));
    }

    /**
     * Create a folder in the current folder and add it to the entry list.
     * @param name the folder name
     */
    public void createFolder(String name) {
        File subFolder = new File(currentFolder, name);

        if (subFolder.mkdir()) {
            FolderEntry entry = new FolderEntry(subFolder);
            folderContent.add(entry);
        }
    }

    /**
     * Create a file in the current folder and add it to the entry list.
     * @param name the file name
     */
    public void createFile(String name) {
        File file = new File(currentFolder, name);

        try {
            if (file.createNewFile()) {
                FileEntry entry = new FileEntry(file);
                folderContent.add(entry);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Set the current folder to one of its child folder if it exists.
     * @param folderName the child folder name
     */
    void enterChildFolder(String folderName) {
        Optional<Entry> entryOptional = this.folderContent.stream()
                .filter(entry -> entry.isDirectory() && entry.getName().equals(folderName))
                .findAny();

        if (entryOptional.isPresent()) {
            FolderEntry entry = (FolderEntry) entryOptional.get();

            this.enterFolder(entry);
        }
    }

    /**
     * Set the current folder and update the entry list accordingly.
     * @param file the folder entry
     */
    public void enterFolder(FolderEntry file) {
        this.currentFolder = file.file;
        this.folderContent.clear();

        this.folderContent.addAll(List.of(file.getEntries()));
    }

    /**
     * Set the current folder and update the entry list accordingly.
     * @param file the folder file
     */
    public void enterFolder(File file) {
        this.enterFolder(new FolderEntry(file));
    }

    /**
     * Set the current folder to its parent folder if it exists.
     */
    public void enterParentFolder() {
        File parent = this.currentFolder.getParentFile();

        if (parent != this.currentFolder && parent != null) {
            this.enterFolder(parent);
        }
    }

    /**
     * Rename a file, remove its previous name from the entry list and adds its new one.
     * @param fileName the new name
     * @param fileToRename the file to rename
     */
    public void renameFile(String fileName, File fileToRename) {
        if (fileToRename.exists()) {
            File file = new File(currentFolder, fileName);

            if (fileToRename.renameTo(file)) {
                folderContent.add(file.isDirectory() ? new FolderEntry(file): new FileEntry(file));
                folderContent.removeIf(entry-> entry.getName(). equals(fileToRename.getName()) );
            } else {
                System.out.println("echec");
            }
        }
    }

    /**
     * Remove a file or a directory from the current folder and remove it from the entry list.
     * @param fileName the file/directory name
     * @return true if successful, false if it failed.
     */
    public boolean deleteFile(String fileName) {
        File fileToDelete = new File(currentFolder, fileName);

        if (fileToDelete.exists()) {
            try {
                if (fileToDelete.isDirectory()) {
                    return deleteDirectory(fileToDelete);
                } else {
                    if (Desktop.isDesktopSupported()
                            && Desktop.getDesktop().isSupported(Desktop.Action.MOVE_TO_TRASH)) {
                        System.out.println("trash");
                        Desktop.getDesktop().moveToTrash(fileToDelete);
                    } else {
                        System.out.println("del");
                        if (fileToDelete.delete()) {
                            folderContent.removeIf(entry -> entry.getName().equals(fileName));
                            return true;
                        } else {
                            System.out.println("Failed to delete the file: " + fileName);
                            return false;
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }
        return false;
    }

    /**
     * Copy a list of files and directories to the clipboard.
     * @param entries files/directories list
     */
    public void copyToClipboard(List<Entry> entries) {
        List<File> files = entries.stream()
                .map(entry -> entry.file)
                .filter(File::exists)
                .toList();

        try {
            Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();

            ClipboardOwner clipboardOwner = (clipBoard, transferable) -> { };

            Transferable transferable = new Transferable() {
                @Override
                public DataFlavor[] getTransferDataFlavors() {
                    return new DataFlavor[]{DataFlavor.javaFileListFlavor};
                }

                @Override
                public boolean isDataFlavorSupported(DataFlavor flavor) {
                    return DataFlavor.javaFileListFlavor.equals(flavor);
                }

                @Override
                public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
                    if (isDataFlavorSupported(flavor)) {
                        return files;
                    } else {
                        throw new UnsupportedFlavorException(flavor);
                    }
                }
            };
            clipboard.setContents(transferable, clipboardOwner);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Paste a list of files or directories from the clipboard into the current folder.
     */
    public void pasteFromClipboard() {
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        Transferable transferable = clipboard.getContents(this);

        if (transferable != null && transferable.isDataFlavorSupported(DataFlavor.javaFileListFlavor)) {
            try {
                List<File> fileList = (List<File>) transferable.getTransferData(DataFlavor.javaFileListFlavor);

                for (File sourceFile : fileList) {
                    String destinationFileName = sourceFile.getName();
                    File destinationFile = new File(currentFolder, destinationFileName);

                    if (sourceFile.isDirectory()) {
                        copyDirectory(sourceFile, destinationFile);
                    } else {
                        copyFile(sourceFile, destinationFile);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Copy a file from a path to another.
     * @param source source path
     * @param destination destination path
     * @return true if successful, false if it failed
     * @throws IOException
     */
    private boolean copyFile(File source, File destination) throws IOException {
        if (destination.isFile()) {
            try {
                Files.copy(source.toPath(), destination.toPath(), StandardCopyOption.REPLACE_EXISTING, StandardCopyOption.COPY_ATTRIBUTES);
                return true;
            } catch (RuntimeException e) {
                return false;
            }
        }

        return false;
    }

    /**
     * Copy a directory and its content from a path to another.
     * @param source source path
     * @param destination destination path
     * @return true if successful, false if it failed
     * @throws IOException
     */
    private boolean copyDirectory(File source, File destination) throws IOException {
        if (source.isDirectory()) {
            if (!destination.exists()) {
                destination.mkdir();
            } else if (!destination.isDirectory()) {
                return false;
            }

            String[] files = source.list();

            if (files != null) {
                for (String file : files) {
                    if (!copyDirectory(new File(source, file), new File(destination, file))) {
                        return false;
                    }
                }
            }

            return true;
        } else {
            return copyFile(source, destination);
        }
    }

    /**
     * Delete a directory and its content.
     * @param directory directory path
     * @return true if successful, false if it failed
     * @throws IOException
     */
    private boolean deleteDirectory(File directory) throws IOException {
        if (directory.isDirectory()) {
            File[] entries = directory.listFiles();
            if (entries != null) {
                for (File entry : entries) {
                    if (entry.isDirectory()) {

                        return deleteDirectory(entry);
                    } else {
                        if (Desktop.isDesktopSupported()
                                && Desktop.getDesktop().isSupported(Desktop.Action.MOVE_TO_TRASH)) {
                            System.out.println("trash");
                            Desktop.getDesktop().moveToTrash(entry);
                        } else {
                            System.out.println("del");
                            if (entry.delete()) {
                                return true;
                            } else {
                                System.out.println("Failed to delete the file: " + entry);
                                return false;
                            }
                        }
                    }
                }
            }

            if (!directory.delete()) {
                System.out.println("Failed to delete the directory: " + directory.getName());
                return false;
            }

            folderContent.removeIf(entry -> entry.getName().equals(directory.getName()));
            return true;
        }
        return false;
    }

    /**
     * Get a copy of the entry list.
     * @return a copy of the entry list.
     */
    private List<Entry> getFolderContent() {
        return new ArrayList<>(folderContent);
    }

    /**
     * Get a copy of the entry list ordered following provided arguments.
     * @param orderingEntry orderings to apply
     * @return a copy of the entry list ordered
     */
    public List<Entry> getEntries(FileOrdering.FileOrderingEntry... orderingEntry) {
        List<Entry> list = this.getFolderContent();

        FileOrdering.order(list, orderingEntry);

        return list;
    }

    /**
     * Get a copy of the entry list with the default ordering, which is by type descending and name ascending.
     * @return a copy of the entry list with the default ordering
     */
    public List<Entry> getEntries() {
        return this.getEntries(new FileOrdering.FileOrderingEntry(FileOrdering.ENTRY_TYPE, true),
                new FileOrdering.FileOrderingEntry(FileOrdering.NAME));
    }

    /**
     * Get a list of commonly accessed directories.
     * @return list of entry
     */
    public List<Entry> getCommonFolders() {
        UserDirectories userDirectories = UserDirectories.get();
        return List.of(
                new FolderEntry(new File(userDirectories.homeDir)),
                new FolderEntry(new File(userDirectories.audioDir)),
                new FolderEntry(new File(userDirectories.desktopDir)),
                new FolderEntry(new File(userDirectories.documentDir)),
                new FolderEntry(new File(userDirectories.downloadDir)),
                new FolderEntry(new File(userDirectories.pictureDir)),
                new FolderEntry(new File(userDirectories.videoDir)),
                new FolderEntry(new File(userDirectories.publicDir)));
    }

    public void openFile(Entry entry) {
        try {
            String[] command = null;

            if (Utils.Os.CURRENT_OS.isWindows()) {
                command = new String[]{"cmd", "-c", entry.file.getAbsolutePath()};
            } else if (Utils.Os.CURRENT_OS.isMacOs()) {
                command = new String[]{"/usr/bin/open", entry.file.getAbsolutePath()};
            } else if (Utils.Os.CURRENT_OS.isLinux()) {
                command = new String[]{"sh", "-c", "/usr/bin/xdg-open '" + entry.file.getAbsolutePath() + "'"};
            }

            if (command != null) {
                Runtime.getRuntime().exec(
                        command
                );
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
