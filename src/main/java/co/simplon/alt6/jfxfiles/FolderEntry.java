package co.simplon.alt6.jfxfiles;

import java.io.File;

public class FolderEntry extends Entry {
    public FolderEntry(File file) {
        super(file);
    }

    @Override
    boolean isDirectory() {
        return true;
    }

    /**
     * Get a folder entry child entries.
     * @return child entries
     */
    public Entry[] getEntries() {
        File[] files = file.listFiles();
        if (files != null) {
            Entry[] entries = new Entry[files.length];
            for (int i = 0; i < files.length; i++) {
                File currentFile = files[i];
                if (currentFile.isDirectory()) {
                    entries[i] = new FolderEntry(currentFile);
                } else {
                    entries[i] = new FileEntry(currentFile);
                }
            }
            return entries;
        } else {
            return null; 
        }
    }
}
