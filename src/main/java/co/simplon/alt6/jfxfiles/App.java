package co.simplon.alt6.jfxfiles;

import co.simplon.alt6.jfxfiles.gui.ExplorerMainPane;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class App extends Application {
    private final Controller controller = new Controller();
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        ExplorerMainPane mainPane = new ExplorerMainPane(controller, stage);
        mainPane.draw();

        Scene scene = new Scene(mainPane, 640, 480);
        stage.setScene(scene);
        stage.show();
    }
}
